package com.rmurugaian.spring.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.stream.IntStream;

/**
 * @author rmurugaian 2019-05-13
 */
@Configuration
public class SecurityConfiguration {

    @Bean
    public PasswordEncoder passwordEncoder() {

        return new BCryptPasswordEncoder();
    }

    public static void main(String[] args) {

        final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        IntStream
            .range(1, 10)
            .mapToObj(it -> "driver0".concat(String.valueOf(it)))
            .peek(System.out::println)
            .map(bCryptPasswordEncoder::encode)
            .forEach(System.out::println);
    }
}
